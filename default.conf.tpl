server {
    # puerto que será reemplazado con el comando envsubst
    listen ${LISTEN_PORT};

    # esta seccion es para servir archivos estáticos, para lo cual nginx es
    # súper eficiente
    location /static {
        alias /vol/static;
    }

    # servimos todo el resto de requests, que irán a la aplicación en django
    location / {
        uwsgi_pass              ${APP_HOST}:${APP_PORT}
        include                 /etc/nginx/uwsgi_params;
        client_max_body_size    10M;
    }
}