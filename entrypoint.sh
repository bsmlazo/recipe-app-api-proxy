#!/bin/sh

# seteamos esto por si ocurrue un error en el escrip, devuelve un error
set -e

# se reemplazan las variables del archivo template y lo dejamos en la
# configuracion por defecto de nginx
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
# para que inicie como un proceso primario y podamos ver los logs en docker
nginx -g 'daemon off'